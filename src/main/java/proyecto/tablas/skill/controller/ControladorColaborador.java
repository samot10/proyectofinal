package proyecto.tablas.skill.controller;

import proyecto.tablas.skill.modelo.Colaborador;
import proyecto.tablas.skill.servicios.ServicioColaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/colaborador")
public class ControladorColaborador {

    @Autowired
    ServicioColaborador servicioColaborador;

    @GetMapping
    public List<Colaborador> obtenerColaborador(){
        return this.servicioColaborador.obtenerColaborador();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Colaborador agregarColaborador(@RequestBody Colaborador colaborador){
        return this.servicioColaborador.ingresarColaborador(colaborador);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Colaborador> soloUnColaborador(@PathVariable String id) {
        Optional<Colaborador> p = this.servicioColaborador.obtenerColaboradorPorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarColaborador(@PathVariable String id){
        this.servicioColaborador.borrarColaboradorPorId(id);
    }

}
