package proyecto.tablas.skill.controller;

import proyecto.tablas.skill.modelo.SkillTech;
import proyecto.tablas.skill.servicios.ServicioSkillTech;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/skillTech")
public class ControladorSkillTech {

    @Autowired
    ServicioSkillTech servicioSkillTech;

    @GetMapping
    public List<SkillTech> obtenerSkillTech(){
        return this.servicioSkillTech.obtenerSkillTech();
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SkillTech agregarSkillTech(@RequestBody SkillTech skillTech){
        return this.servicioSkillTech.ingresarSkillTech(skillTech);
    }
    @GetMapping("/{id}")
    public ResponseEntity<SkillTech> soloUnSkillTech(@PathVariable String id) {
        Optional<SkillTech> p = this.servicioSkillTech.obtenerSkillTechPorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSkillTech(@PathVariable String id){
        this.servicioSkillTech.borrarSkillTechPorId(id);
}

}
