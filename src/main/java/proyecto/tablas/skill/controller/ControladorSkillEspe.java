package proyecto.tablas.skill.controller;

import proyecto.tablas.skill.modelo.SkillEspe;
import proyecto.tablas.skill.servicios.ServicioSkillEspe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/skillEspe")
public class ControladorSkillEspe {

    @Autowired
    ServicioSkillEspe servicioSkillEspe;

    @GetMapping
    public List<SkillEspe> obtenerSkillEspe(){
        return this.servicioSkillEspe.obtenerSkillEspe();
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SkillEspe agregarSkillEspe(@RequestBody SkillEspe skillEspe){
        return this.servicioSkillEspe.ingresarSkillEspe(skillEspe);
    }
    @GetMapping("/{id}")
    public ResponseEntity<SkillEspe> soloUnSkillEspe(@PathVariable String id) {
        Optional<SkillEspe> p = this.servicioSkillEspe.obtenerSkillEspePorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSkillEspe(@PathVariable String id){
        this.servicioSkillEspe.borrarSkillEspePorId(id);
}

}
