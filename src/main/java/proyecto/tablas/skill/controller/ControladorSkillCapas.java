package proyecto.tablas.skill.controller;

import proyecto.tablas.skill.modelo.SkillCapas;
import proyecto.tablas.skill.servicios.ServicioSkillCapas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/skillCapas")
public class ControladorSkillCapas {

    @Autowired
    ServicioSkillCapas servicioSkillCapas;

    @GetMapping
    public List<SkillCapas> obtenerSkillCapas(){
        return this.servicioSkillCapas.obtenerSkillCapas();
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SkillCapas agregarSkillCapas(@RequestBody SkillCapas skillCapas){
        return this.servicioSkillCapas.ingresarSkillCapas(skillCapas);
    }
    @GetMapping("/{id}")
    public ResponseEntity<SkillCapas> soloUnSkillCapas(@PathVariable String id) {
        Optional<SkillCapas> p = this.servicioSkillCapas.obtenerSkillCapasPorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSkillCapas(@PathVariable String id){
        this.servicioSkillCapas.borrarSkillCapasPorId(id);
}

}
