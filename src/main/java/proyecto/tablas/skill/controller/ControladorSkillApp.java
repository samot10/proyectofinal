package proyecto.tablas.skill.controller;

import proyecto.tablas.skill.modelo.SkillApp;
import proyecto.tablas.skill.servicios.ServicioSkillApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/aplicativos")
public class ControladorSkillApp {

    @Autowired
    ServicioSkillApp servicioSkillApp;

    @GetMapping
    public List<SkillApp> obtenerSkillApp(){
        return this.servicioSkillApp.obtenerSkillApp();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SkillApp agregarSkillApp(@RequestBody SkillApp skillApp){
        return this.servicioSkillApp.ingresarSkillApp(skillApp);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SkillApp> soloUnSkillApp(@PathVariable String id) {
        Optional<SkillApp> p = this.servicioSkillApp.obtenerSkillAppPorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSkillApp(@PathVariable String id){
        this.servicioSkillApp.borrarSkillAppPorId(id);
    }

}
