package proyecto.tablas.skill.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TipTech")
public class SkillTech {
    @Id String id;
    String idTipoSkill;
    String Descripcion;
    String Estado;

    public SkillTech(String id, String idTipoSkill, String descripcion, String estado) {
        this.id = id;
        this.idTipoSkill = idTipoSkill;
        Descripcion = descripcion;
        Estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdTipoSkill() {
        return idTipoSkill;
    }

    public void setIdTipoSkill(String idTipoSkill) {
        this.idTipoSkill = idTipoSkill;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }
}
