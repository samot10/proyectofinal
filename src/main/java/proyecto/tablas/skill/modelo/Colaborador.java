package proyecto.tablas.skill.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "colaborador")
public class Colaborador {
    @Id String id;
    String Codigo;
    String Nombres;
    String email;
    String Fabrica;
    String Estado;

    public Colaborador(String id, String codigo, String nombres, String email, String fabrica, String estado) {
        this.id = id;
        Codigo = codigo;
        Nombres = nombres;
        this.email = email;
        Fabrica = fabrica;
        Estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFabrica() {
        return Fabrica;
    }

    public void setFabrica(String fabrica) {
        Fabrica = fabrica;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

}
