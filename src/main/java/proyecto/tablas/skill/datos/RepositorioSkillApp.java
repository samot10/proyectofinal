package proyecto.tablas.skill.datos;

import proyecto.tablas.skill.modelo.SkillApp;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioSkillApp extends MongoRepository<SkillApp, String> {
}
