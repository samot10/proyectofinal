package proyecto.tablas.skill.datos;

import proyecto.tablas.skill.modelo.SkillTech;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioSkillTech extends MongoRepository<SkillTech, String> {
}
