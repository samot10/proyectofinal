package proyecto.tablas.skill.datos;

import proyecto.tablas.skill.modelo.SkillEspe;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioSkillEspe extends MongoRepository<SkillEspe, String> {
}
