package proyecto.tablas.skill.datos;

import proyecto.tablas.skill.modelo.Colaborador;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioColaborador extends MongoRepository<Colaborador, String> {
}
