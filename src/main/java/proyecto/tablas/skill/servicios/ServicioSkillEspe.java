package proyecto.tablas.skill.servicios;

import proyecto.tablas.skill.modelo.SkillEspe;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioSkillEspe {

    public SkillEspe ingresarSkillEspe(SkillEspe skillEspe);
    public List<SkillEspe> obtenerSkillEspe();
    public Optional<SkillEspe> obtenerSkillEspePorId(String id);
    public SkillEspe actualizaSkillEspe(SkillEspe skillEspe);
    public void borrarSkillEspePorId(String id);

}
