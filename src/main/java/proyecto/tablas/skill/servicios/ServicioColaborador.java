package proyecto.tablas.skill.servicios;

import proyecto.tablas.skill.modelo.Colaborador;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioColaborador {

    public Colaborador ingresarColaborador(Colaborador Colaborador);
    public List<Colaborador> obtenerColaborador();
    public Optional<Colaborador> obtenerColaboradorPorId(String id);
    public Colaborador actualizaColaborador(Colaborador Colaborador);
    public void borrarColaboradorPorId(String id);

}
