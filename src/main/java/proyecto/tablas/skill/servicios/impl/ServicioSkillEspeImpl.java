package proyecto.tablas.skill.servicios.impl;

import proyecto.tablas.skill.datos.RepositorioSkillEspe;
import proyecto.tablas.skill.modelo.SkillEspe;
import proyecto.tablas.skill.servicios.ServicioSkillEspe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioSkillEspeImpl implements ServicioSkillEspe{

    public ServicioSkillEspeImpl(){
        System.out.println("======== ServicioSkillEspeImp");
    }

    @Autowired
    private RepositorioSkillEspe repositorioSkillEspe;

    @Override
    public SkillEspe ingresarSkillEspe(SkillEspe skillEspe) {
        return this.repositorioSkillEspe.insert(skillEspe);
    }

    @Override
    public List<SkillEspe> obtenerSkillEspe() {
        return this.repositorioSkillEspe.findAll();
    }

    @Override
    public Optional<SkillEspe> obtenerSkillEspePorId(String id) {
        return this.repositorioSkillEspe.findById(id);
    }

    @Override
    public SkillEspe actualizaSkillEspe(SkillEspe skillEspe) {
        return this.repositorioSkillEspe.save(skillEspe);
    }

    @Override
    public void borrarSkillEspePorId(String id) {
        this.repositorioSkillEspe.deleteById(id);
    }

}
