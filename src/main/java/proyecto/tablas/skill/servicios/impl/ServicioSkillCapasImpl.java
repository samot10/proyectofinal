package proyecto.tablas.skill.servicios.impl;

import proyecto.tablas.skill.datos.RepositorioSkillCapas;
import proyecto.tablas.skill.modelo.SkillCapas;
import proyecto.tablas.skill.servicios.ServicioSkillCapas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioSkillCapasImpl implements ServicioSkillCapas{

    public ServicioSkillCapasImpl(){
        System.out.println("======== ServicioSkillCapasImp");
    }

    @Autowired
    private RepositorioSkillCapas repositorioSkillCapas;

    @Override
    public SkillCapas ingresarSkillCapas(SkillCapas skillCapas) {
        return this.repositorioSkillCapas.insert(skillCapas);
    }

    @Override
    public List<SkillCapas> obtenerSkillCapas() {
        return this.repositorioSkillCapas.findAll();
    }

    @Override
    public Optional<SkillCapas> obtenerSkillCapasPorId(String id) {
        return this.repositorioSkillCapas.findById(id);
    }

    @Override
    public SkillCapas actualizaSkillCapas(SkillCapas skillCapas) {
        return this.repositorioSkillCapas.save(skillCapas);
    }

    @Override
    public void borrarSkillCapasPorId(String id) {
        this.repositorioSkillCapas.deleteById(id);
    }

}
