package proyecto.tablas.skill.servicios.impl;

import proyecto.tablas.skill.datos.RepositorioSkillApp;
import proyecto.tablas.skill.modelo.SkillApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proyecto.tablas.skill.servicios.ServicioSkillApp;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioSkillAppImpl implements ServicioSkillApp {

    public ServicioSkillAppImpl(){
        System.out.println("======== ServicioSkillAppImp");
    }

    @Autowired
    private RepositorioSkillApp repositorioSkillApp;

    @Override
    public SkillApp ingresarSkillApp(SkillApp skillApp) {
        return this.repositorioSkillApp.insert(skillApp);
    }

    @Override
    public List<SkillApp> obtenerSkillApp() {
        return this.repositorioSkillApp.findAll();
    }

    @Override
    public Optional<SkillApp> obtenerSkillAppPorId(String id) {
        return this.repositorioSkillApp.findById(id);
    }

    @Override
    public SkillApp actualizaSkillApp(SkillApp skillApp) {
        return this.repositorioSkillApp.save(skillApp);
    }

    @Override
    public void borrarSkillAppPorId(String id) {
        this.repositorioSkillApp.deleteById(id);
    }
    
}
