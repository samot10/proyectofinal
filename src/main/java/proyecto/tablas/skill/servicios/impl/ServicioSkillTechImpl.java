package proyecto.tablas.skill.servicios.impl;

import proyecto.tablas.skill.datos.RepositorioSkillTech;
import proyecto.tablas.skill.modelo.SkillTech;
import proyecto.tablas.skill.servicios.ServicioSkillTech;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioSkillTechImpl implements ServicioSkillTech{

    public ServicioSkillTechImpl(){
        System.out.println("======== ServicioSkillTechImp");
    }

    @Autowired
    private RepositorioSkillTech repositorioSkillTech;

    @Override
    public SkillTech ingresarSkillTech(SkillTech skillTech) {
        return this.repositorioSkillTech.insert(skillTech);
    }

    @Override
    public List<SkillTech> obtenerSkillTech() {
        return this.repositorioSkillTech.findAll();
    }

    @Override
    public Optional<SkillTech> obtenerSkillTechPorId(String id) {
        return this.repositorioSkillTech.findById(id);
    }

    @Override
    public SkillTech actualizaSkillTech(SkillTech skillTech) {
        return this.repositorioSkillTech.save(skillTech);
    }

    @Override
    public void borrarSkillTechPorId(String id) {
        this.repositorioSkillTech.deleteById(id);
    }

}
