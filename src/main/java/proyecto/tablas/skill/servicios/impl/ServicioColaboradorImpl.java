package proyecto.tablas.skill.servicios.impl;

import proyecto.tablas.skill.datos.RepositorioColaborador;
import proyecto.tablas.skill.modelo.Colaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proyecto.tablas.skill.servicios.ServicioColaborador;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioColaboradorImpl implements ServicioColaborador {

    public ServicioColaboradorImpl(){
        System.out.println("======== ServicioColaboradorImp");
    }

    @Autowired
    private RepositorioColaborador repositorioColaborador;

    @Override
    public Colaborador ingresarColaborador(Colaborador Colaborador) {
        return this.repositorioColaborador.insert(Colaborador);
    }

    @Override
    public List<Colaborador> obtenerColaborador() {
        return this.repositorioColaborador.findAll();
    }

    @Override
    public Optional<Colaborador> obtenerColaboradorPorId(String id) {
        return this.repositorioColaborador.findById(id);
    }

    @Override
    public Colaborador actualizaColaborador(Colaborador Colaborador) {
        return this.repositorioColaborador.save(Colaborador);
    }

    @Override
    public void borrarColaboradorPorId(String id) {
        this.repositorioColaborador.deleteById(id);
    }

}
