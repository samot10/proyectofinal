package proyecto.tablas.skill.servicios;

import proyecto.tablas.skill.modelo.SkillTech;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioSkillTech {

    public SkillTech ingresarSkillTech(SkillTech skillTech);
    public List<SkillTech> obtenerSkillTech();
    public Optional<SkillTech> obtenerSkillTechPorId(String id);
    public SkillTech actualizaSkillTech(SkillTech skillTech);
    public void borrarSkillTechPorId(String id);

}
