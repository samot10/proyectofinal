package proyecto.tablas.skill.servicios;

import proyecto.tablas.skill.modelo.SkillApp;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioSkillApp {

    public SkillApp ingresarSkillApp(SkillApp skillApp);
    public List<SkillApp> obtenerSkillApp();
    public Optional<SkillApp> obtenerSkillAppPorId(String id);
    public SkillApp actualizaSkillApp(SkillApp skillApp);
    public void borrarSkillAppPorId(String id);

}
