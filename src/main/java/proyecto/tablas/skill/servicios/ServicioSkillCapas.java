package proyecto.tablas.skill.servicios;

import proyecto.tablas.skill.modelo.SkillCapas;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioSkillCapas {

    public SkillCapas ingresarSkillCapas(SkillCapas skillCapas);
    public List<SkillCapas> obtenerSkillCapas();
    public Optional<SkillCapas> obtenerSkillCapasPorId(String id);
    public SkillCapas actualizaSkillCapas(SkillCapas skillCapas);
    public void borrarSkillCapasPorId(String id);

}
